import axios from 'axios';

class MeetUp {

  getMeetUps () {
    axios.get('/meetupAPI')
      .then(response => {
        return response.data;
      })
      .catch(e => {
        return [];
      });
  }
}

export default new MeetUp();
