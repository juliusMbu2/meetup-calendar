import Vue from 'vue';
import Router from 'vue-router';
import MeetupCalendar from '@/components/MeetupCalendar';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MeetUp Calendar',
      component: MeetupCalendar
    }
  ]
});
