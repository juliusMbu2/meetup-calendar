import { shallow } from 'vue-test-utils';
import moxios from 'moxios';
import MeetupCalendar from '@/components/MeetupCalendar';

window.Promise = require('promise');

describe('MeetupCalendar.vue', () => {
  const card = {
    'created': 1510322902000,
    'duration': 9000000,
    'id': '244995704',
    'name': 'Kanban for ITSM',
    'status': 'upcoming',
    'time': 1511202600000,
    'local_date': '2017-11-20',
    'local_time': '18:30',
    'updated': 1510322902000,
    'utc_offset': 0,
    'waitlist_count': 0,
    'yes_rsvp_count': 109,
    'venue': {
      'id': 24281790,
      'name': 'Valtech',
      'lat': 53.48418426513672,
      'lon': -2.2384209632873535,
      'repinned': false,
      'address_1': 'Basil Chambers. 65 High Street. M4 1FS',
      'city': 'Manchester',
      'country': 'gb',
      'localized_country_name': 'United Kingdom'
    },
    'group': {
      'created': 1309793494000,
      'name': 'Lean Agile, Manchester',
      'id': 2119921,
      'join_mode': 'open',
      'lat': 53.47999954223633,
      'lon': -2.240000009536743,
      'urlname': 'Lean-Agile-Manchester',
      'who': 'Members',
      'localized_location': 'Manchester, United Kingdom',
      'region': 'en_US'
    },
    'link': 'https://www.meetup.com/Lean-Agile-Manchester/events/244995704/',
    'description': 'test',
    'visibility': 'public'
  };

  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('should render correct cards', done => {
    moxios.stubRequest('/meetupAPI', {
      status: 200,
      response: [card, [card, card]]
    });

    const cmp = shallow(MeetupCalendar);

    moxios.wait(function () {
      expect(cmp.vm.cards.length).to.equal(2);
      expect(cmp.vm.cards[0].time).to.equal('Monday');
      expect(cmp.vm.cards[1][0].time).to.equal('Monday');
      done();
    });
  });

  it('should add errors on failure', done => {
    moxios.stubRequest('/meetupAPI', {
      status: 500,
      response: { error: 'error' }
    });

    const cmp = shallow(MeetupCalendar);

    moxios.wait(() => {
      expect(cmp.vm.cards.length).to.equal(0);
      expect(cmp.vm.errors[0].response.data.error).to.equal('error');
      done();
    });
  });
});
