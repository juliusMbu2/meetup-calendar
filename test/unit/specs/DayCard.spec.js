import DayCard from '@/components/DayCard';
import { shallow } from 'vue-test-utils';

describe('DayCard.vue', () => {
  const card = {
    'name': 'Kanban for ITSM',
    'time': 1511202600000,
    'local_date': '2017-11-20',
    'local_time': '18:30',
    'yes_rsvp_count': 109,
    'venue': {
      'name': 'Valtech',
      'address_1': 'Basil Chambers. 65 High Street. M4 1FS'
    }
  };

  it('should show meetups', () => {
    const cards = [card, card, [card, card], card];
    const cmp = shallow(DayCard, {
      propsData: { cards }
    });

    expect(cmp.findAll('.card').length).to.equal(cards.length - 1);
    expect(cmp.findAll('.card').at(1).findAll('.card-list-item').length).to.equal(2);
  });
});
