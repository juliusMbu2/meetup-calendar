// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  '@tags': ['car-insurance-home'],
  'default e2e Meetup Calendar landing page': (browser) => {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js

    const testServer = browser.globals.herokuServerURL;
    browser
      .url(testServer)
      .waitForElementVisible('#app', 5000)
      .waitForElementVisible('body', 2000)
      .waitForElementVisible('.card', 2000)
      .end();
  }

//   const testServer = browser.globals.testServerURL;
// browser
//   .url(testServer + '/car-insurance')
//   .waitForElementVisible('.motorinsurance-body', 5000)
//   .assert.elementPresent('.motorinsurance-body')
//   .assert.elementPresent('.msm-b-bm__content-holder')
//   .end();
// }
  // 'Travel to car insurance for MSM homepage': function (browser) {
  //   const testServer = browser.globals.testServerURL;
  //   browser
  //     .url(testServer)
  //     .waitForElementVisible('#house-mainnav-car-insurance', 5000)
  //     .assert.elementPresent('#house-mainnav')
  //     .click('#house-mainnav-car-insurance', 4000)
  //     .wait(5000)
  //     .browser.assert.title('Compare Cheap Car Insurance Quotes - MoneySuperMarket')
  //     .end();
  // },
  // 'Travel to questionset page HighImpactQuestion': function (browser) {
  //   const testServer = browser.globals.testServerURL;
  //   browser
  //     .url(testServer + '/car-insurance')
  //     .waitForElementVisible('.motorinsurance-body', 5000)
  //     .assert.elementPresent('.motorinsurance-body')
  //     .assert.elementPresent('.msm-b-bm__content-holder')
  //     .click('[title="Get a brand new quote"]')
  //     .wait(5000)
  //     .browser.assert.title('Compare Cheap Car Insurance Quotes - MoneySuperMarket')
  //     .end();
  // },
  // 'Travel to meetup calender homepage': function (browser) {
  //   browser
  //     .url(testServer)
  //     .waitForElementVisible('#house-mainnav-car-insurance', 5000)
  //     .assert.elementPresent('#house-mainnav')
  //     .click('#house-mainnav-car-insurance', 4000)
  //     .wait(5000)
  //     .browser.assert.title('Compare Cheap Car Insurance Quotes - MoneySuperMarket')
  //     .end();
  // }
};
