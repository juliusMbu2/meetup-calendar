require('babel-register');
var config = require('../../config');
// const phantomjs = require('phantomjs-prebuilt');
// const chromedriver = require('chromedriver');

// http://nightwatchjs.org/gettingstarted#settings-file
module.exports = {
  src_folders: ['test/e2e/specs'],
  output_folder: 'test/e2e/reports',
  custom_assertions_path: ['test/e2e/custom-assertions'],

  selenium: {
    start_process: true,
    server_path: require('selenium-server').path,
    host: '127.0.0.1',
    port: 4444,
    cli_args: {
      'webdriver.chrome.driver': require('chromedriver').path
    }
  },

  test_settings: {
    default: {
      selenium_port: 4444,
      selenium_host: 'localhost',
      silent: true,
      globals: {
        testServerURL: 'http://localhost:' + (process.env.PORT || config.dev.port),
        herokuServerURL: 'https://msm-meetup-calendar2.herokuapp.com'
        // devServerURL: 'http://localhost:' + (process.env.PORT || config.dev.port)
      }
    },

    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    },

    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true
      }
    }
  }};
//
// const seleniumServer = require('selenium-server')
// const phantomjs = require('phantomjs-prebuilt')
// // const chromedriver = require('chromedriver')
//
// module.exports = {
//   src_folders: ['test/e2e/specs'],
//   output_folder: 'test/e2e/reports',
//   custom_assertions_path: 'test/e2e/custom-assertions',
//   live_output: false,
//   disable_colors: false,
//   selenium: {
//     start_process: false,
//     server_path: seleniumServer.path,
//     log_path: '',
//     host: '127.0.0.1',
//     port: 4444
//   },
//   test_settings: {
//     default: {
//       herokuServerURL: 'https://msm-meetup-calendar2.herokuapp.com',
//       selenium_port: 4444,
//       selenium_host: '127.0.0.1',
//       desiredCapabilities: {
//         browserName: 'phantomjs',
//         javascriptEnabled: true,
//         acceptSslCerts: true,
//         'phantomjs.binary.path': 'test/globals/phantomjs-2.1.1-macosx/bin/phantomjs.jar',
//         'phantomjs.cli.args': '--ignore-ssl-errors=true'
//       }
//     },
//     // chrome: {
//     //   desiredCapabilities: {
//     //     browserName: 'chrome',
//     //     javascriptEnabled: true,
//     //     acceptSslCerts: true
//     //   },
//     //   selenium: {
//     //     cli_args: {
//     //       'webdriver.chrome.driver': chromedriver.path
//     //     }
//     //   }
//     // },
//     firefox: {
//       desiredCapabilities: {
//         browserName: 'firefox',
//         javascriptEnabled: true,
//         acceptSslCerts: true
//       }
//     }
//   }
// };


